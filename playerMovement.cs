﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    Rigidbody2D rgbd;
    Vector3 vel;
    float speedConst= 5f;
    float jumpConst= 3f;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        vel = new Vector3(Input.GetAxis("Horizontal"), 0f);
        transform.position += vel * speedConst * Time.deltaTime;
        if (Input.GetButtonDown("Jump")&& Mathf.Approximately(rgbd.velocity.y, 0))
        {
            rgbd.AddForce(Vector3.up * jumpConst, ForceMode2D.Impulse);
        }
        if(Input.GetAxisRaw("Horizontal")==-1)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);

        }
        else if (Input.GetAxisRaw("Horizontal") == 1)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        }
    }
}
